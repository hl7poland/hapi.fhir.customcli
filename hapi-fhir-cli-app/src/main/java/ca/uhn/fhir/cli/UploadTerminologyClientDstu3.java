package ca.uhn.fhir.cli;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.hl7.fhir.instance.model.api.IBaseParameters;
import org.hl7.fhir.dstu3.model.*;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.jpa.term.IHapiTerminologyLoaderSvc;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.BearerTokenAuthInterceptor;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;


public class UploadTerminologyClientDstu3 extends VersionSpecificClient {
	private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(UploadTerminologyClientDstu3.class);

	@Override
	public void run(String targetUrl, CommandLine parameters) throws ParseException, Exception {
		FhirContext ctx = new FhirContext(FhirVersionEnum.DSTU3);
		
		String termUrl = parameters.getOptionValue("u");
		if (isBlank(termUrl)) {
			throw new ParseException("No URL provided");
		}
		
		String[] datafile = parameters.getOptionValues("d");
		if (datafile == null || datafile.length == 0) {
			throw new ParseException("No data file provided");
		}
		
		String bearerToken = parameters.getOptionValue("b");
		IGenericClient client = super.newClient(ctx, targetUrl);
		IBaseParameters inputParameters;
		
		Parameters p = new Parameters();
		p.addParameter().setName("url").setValue(new UriType(termUrl));
		for (String next : datafile) {
			p.addParameter().setName("localfile").setValue(new StringType(next));
		}
		inputParameters = p;
		
		if (isNotBlank(bearerToken)) {
			client.registerInterceptor(new BearerTokenAuthInterceptor(bearerToken));
		}

		if (parameters.hasOption('v')) {
			client.registerInterceptor(new LoggingInterceptor(true));
		}
		
		ourLog.info("Beginning upload - This may take a while...");
		IBaseParameters response = client
			.operation()
			.onServer()
			.named("upload-external-code-system")
			.withParameters(inputParameters)
			.execute();

		ourLog.info("Upload complete!");
		ourLog.info("Response:\n{}", ctx.newXmlParser().setPrettyPrint(true).encodeResourceToString(response));


	}

}
