package ca.uhn.fhir.cli;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;

public abstract class VersionSpecificClient {
	
	protected IGenericClient newClient(FhirContext ctx, String targetUrl) {
		ctx.getRestfulClientFactory().setSocketTimeout(10 * 60 * 1000);
		IGenericClient fhirClient = ctx.newRestfulGenericClient(targetUrl);
		return fhirClient;
	}
	
	public abstract void run(String targetUrl, CommandLine parameters) throws ParseException, Exception;
}
