package ca.uhn.fhir.cli;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import org.apache.commons.cli.*;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseParameters;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.context.FhirVersionEnum;
import ca.uhn.fhir.jpa.term.IHapiTerminologyLoaderSvc;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.interceptor.BearerTokenAuthInterceptor;
import ca.uhn.fhir.rest.client.interceptor.LoggingInterceptor;

public class UploadTerminologyCommand extends BaseCommand {

	private static final org.slf4j.Logger ourLog = org.slf4j.LoggerFactory.getLogger(UploadTerminologyCommand.class);

	@Override
	public String getCommandDescription() {
		return "Uploads a terminology package (e.g. a SNOMED CT ZIP file) to a HAPI JPA server. "
				+ "Note that this command uses a custom operation that is only implemented on HAPI "
				+ "JPA servers that have been configured to accept it.";
	}

	@Override
	public String getCommandName() {
		return "upload-terminology";
	}

	@Override
	public Options getOptions() {
		Options options = new Options();
		Option opt;

		addFhirVersionOption(options);

		opt = new Option("t", "target", true, "Base URL for the target server (e.g. \"http://example.com/fhir\")");
		opt.setRequired(true);
		options.addOption(opt);

		opt = new Option("u", "url", true, "The code system URL associated with this upload (e.g. " + IHapiTerminologyLoaderSvc.SCT_URL + ")");
		opt.setRequired(false);
		options.addOption(opt);

		opt = new Option("d", "data", true, "Local *.zip containing file to use to upload");
		opt.setRequired(false);
		options.addOption(opt);

		opt = new Option("b", "bearer-token", true, "Bearer token to add to the request");
		opt.setRequired(false);
		options.addOption(opt);

		opt = new Option("v", "verbose", false, "Verbose output");
		opt.setRequired(false);
		options.addOption(opt);
		
		return options;
	}

	@Override
	public void run(CommandLine theCommandLine) throws Exception {
		String targetServer = theCommandLine.getOptionValue("t");
		if (isBlank(targetServer)) {
			throw new ParseException("No target server (-t) specified");
		} else if (targetServer.startsWith("http") == false && targetServer.startsWith("file") == false) {
			throw new ParseException("Invalid target server specified, must begin with 'http' or 'file'");
		}
		//TODO: Version specific client call
		String specVersion = theCommandLine.getOptionValue("f");
		if ("dstu3".equals(specVersion)) {
			new UploadTerminologyClientDstu3().run(targetServer, theCommandLine);
		} else if ("r4".equals(specVersion)) {
			new UploadTerminologyClientR4().run(targetServer, theCommandLine);
		} else {
			throw new ParseException("Unsupported spec version: " + specVersion);
		}
	}

}
